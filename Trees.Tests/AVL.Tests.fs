module Trees.AVL.Tests

open Expecto
open AVL
open Node

let extractTreeNode node = 
  match node with
  | TreeNode (v, l, r) -> (v, l, r)
  | _ -> failwith "Invalid Node type" "Only TreeNode (T * TreeNode * TreeNode) are supported"

[<Tests>]
let tests = 
  testList "AVL Tree" [
    testList "Add" [
      test "Added value can be found" {
        let tree = create "B" |> add "A" |> add "C" |> add "D"
        let result = search "D" tree
        
        Expect.isSome result "No result found for search 'D'"
      }

      test "Remains balanced after initial addition" {
        let tree = create "B" |> add "A"
        let nextTree = add "C" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced far left (AKA LL, AKA Single Left Rotation)" {
        let tree = create "A" |> add "B"
        let nextTree = add "C" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced somewhat left (AKA LR, AKA Double Left Rotation)" {
        let tree = create "A" |> add "C"
        let nextTree = add "B" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced far right (AKA RR, AKA Single Right Rotation)" {
        let tree = create "C" |> add "B"
        let nextTree = add "A" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced somewhat right (AKA RL, AKA Double Right Rotation)" {
        let tree = create "C" |> add "A"
        let nextTree = add "B" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }
      
      test "Larger trees will be maintain the height invariant" {
        let tree = create "C" |> add "A" |> add "B" |> add "F" |> add "E"
        let nextTree = add "D" tree
        let (_, firstLeft, firstRight) = extractTreeNode tree
        let (_, nextLeft, nextRight) = extractTreeNode nextTree

        Expect.equal (height firstLeft) 1 "The left subtree is the wrong height after 'E' was added"
        Expect.equal (height firstRight) 2 "The right subtree is the wrong height after 'E' was added"

        Expect.equal (height nextLeft) 2 "The left subtree is the wrong height after 'D' was added"
        Expect.equal (height nextRight) 2 "The right subtree is the wrong height after 'D' was added"
      }
    ]

    testList "Height" [
      test "Single node (1) tree has a height of 1" {
        let node = create "Test Value"

        Expect.equal (AVL.height node) 1 "Single node has height zero"
      }

      test "Multi node (3) tree | one (1) right of the root and one (1) left of the root)" {
        let node = create "B" |> add "C" |> add "A"

        Expect.equal (AVL.height node) 2 "Equal subtree heights should be 2"
      }

      test "Multi node (5) tree | three (3) left of the root and one (1) right of the root" {
        let node = create "D" |> add "B" |> add "C" |> add "A" |> add "E"

        Expect.equal (AVL.height node) 3 "Multi-node height should be 3"
      }

      test "Multi node (3) tree | three (3) right of the root and one (1) left of the root" {
        let node = create "B" |> add "A" |> add "D" |> add "C" |> add "E"

        Expect.equal (AVL.height node) 3 "Multi-node height should be 3"
      }
    ]

    testList "Remove" [
      test "Value cannot be found after removal" {
        let tree = create "B" |> add "A" |> add "C"
        let nextTree = remove "C" tree

        Expect.isNone (search "C" nextTree) "Value found after attempting to remove 'C'"
      }

      test "Remains balanced after initial removal" {
        let tree = create "B" |> add "A" |> add "C"
        let nextTree = remove "C" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.equal (height tree) (height nextTree) "Old tree and new tree should have same height"
        Expect.isGreaterThan (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced far left (AKA LL, AKA Single Left Rotation)" {
        let tree = create "A" |> add "B" |> add "C" |> add "D"
        let nextTree = remove "A" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.isGreaterThan (height tree) (height nextTree) "Height of old tree <= Height of next tree"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced somewhat left (AKA LR, AKA Double Left Rotation)" {
        let tree = create "B" |> add "A" |> add "D" |> add "C"
        let nextTree = remove "A" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.isGreaterThan (height tree) (height nextTree) "Height of old tree <= Height of next tree"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced far right (AKA RR, AKA Single Right Rotation)" {
        let tree = create "C" |> add "D" |> add "B" |> add "A"
        let nextTree = remove "D" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.isGreaterThan (height tree) (height nextTree) "Height of old tree <= Height of next tree"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }

      test "Becomes unbalanced somewhat right (AKA RL, AKA Double Right Rotation)" {
        let tree = create "C" |> add "D" |> add "A" |> add "B"
        let nextTree = remove "D" tree
        let (_, l, r) = extractTreeNode nextTree

        Expect.isGreaterThan (height tree) (height nextTree) "Height of old tree <= Height of next tree"
        Expect.equal (height l) (height r) "Balanced tree should have equal height"
      }
      
      test "Larger trees will be maintain the height invariant" {
        let tree = create "C" |> add "A" |> add "E" |> add "B" |> add "D" |> add "F" |> add "H"
        let nextTree = remove "A" tree
        let (_, firstLeft, firstRight) = extractTreeNode tree
        let (_, nextLeft, nextRight) = extractTreeNode nextTree

        Expect.equal (height firstLeft) 2 "The left subtree is the wrong height after 'H' was added"
        Expect.equal (height firstRight) 3 "The right subtree is the wrong height after 'H' was added"

        Expect.equal (height nextLeft) 2 "The left subtree is the wrong height after 'A' was removed"
        Expect.equal (height nextRight) 2 "The right subtree is the wrong height after 'A' was removed"
      }
    ]
  ]