module Trees

open Order
open BST

[<EntryPoint>]
let main argv =
    printfn "Testing with some letters (create/add/find):"
    printfn "----------------------------------------------------------"

    let root = create "M" |> add "A" |> add "Z" |> add "B" |> add "Y"
    printfn "Initial tree:"
    print root Order.INFIX

    printfn "\nFind 'Z' in tree:"
    printfn "%s" <| Option.get (BST.search "Z" root)

    printfn "\nFind 'L' in tree:"
    printfn "%s" <| match (BST.search "L" root) with None -> "NONE" | _ -> "This is bad..."

    printfn "==========================================================="
    printfn "Test with some numbers (create/add/remove)"
    printfn "----------------------------------------------------------"
    
    let root = create 1000 |> add 5 |> add 3 |> add 4 |> add 21 |> add 30 |> add 20 |> add 19
    printfn "Initial tree:"
    print root Order.INFIX

    printfn "\nRemove 4 from tree:"
    printfn "%A" <| remove 4 root

    printfn "\nRemove 3 from tree:"
    printfn "%A" <| remove 3 root

    printfn "\nRemove 5 from tree:"
    printfn "%A" <| remove 5 root

    0 // return an integer exit code
