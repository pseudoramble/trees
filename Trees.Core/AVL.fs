module AVL

open Node
open Order

let rec height tree =
  match tree with
  | Leaf -> 0
  | TreeNode (_, l, r) -> 1 + max (height l) (height r)

let rotateLeft node =
  let (rootValue, rootLeft, rootRight) = node

  match rootRight with
  | Leaf -> node
  | TreeNode (rValue, rLeft, rRight) ->
    (rValue, TreeNode (rootValue, rootLeft, rLeft), rRight)

let rotateRight node =
  let (rootValue, rootLeft, rootRight) = node

  match rootLeft with
  | Leaf -> node
  | TreeNode (lValue, lLeft, lRight) ->
    (lValue, lLeft, TreeNode (rootValue, lRight, rootRight))

let rec rightRotationStrategy node =
  let (rootValue, rootLeft, rootRight) = node

  match rootLeft with
  | Leaf -> node
  | TreeNode (leftValue, lLeftSubtree, lRightSubtree) ->
    if (height lLeftSubtree) - (height lRightSubtree) > 0 then 
      rotateRight node
    else
      rotateRight (rootValue, TreeNode (rotateLeft (leftValue, lLeftSubtree, lRightSubtree)), rootRight)

let rec leftRotationStrategy node =
  let (rootValue, rootLeft, rootRight) = node
  
  match rootRight with
  | Leaf -> node
  | TreeNode (rightValue, rLeftSubtree, rRightSubtree) ->
    if (height rLeftSubtree) - (height rRightSubtree) < 0 then
      rotateLeft node
    else
      rotateLeft (rootValue, rootLeft, TreeNode (rotateRight (rightValue, rLeftSubtree, rRightSubtree)))

let rec add value tree =
  match tree with
  | Leaf -> TreeNode(value, Leaf, Leaf)
  | TreeNode (v, left, right) as node ->
      let nextTree = 
        if value < v then TreeNode (v, add value left, right)
        elif value > v then TreeNode (v, left, add value right)
        else node

      match nextTree with
      | Leaf -> nextTree
      | TreeNode (v, l, r) ->
        let heightDiff = (height l) - (height r)

        if heightDiff > 1 then TreeNode (rightRotationStrategy (v, l, r))
        elif heightDiff < -1 then TreeNode (leftRotationStrategy (v, l, r))
        else nextTree

let create value =
  TreeNode (value, Leaf, Leaf)

let rec max tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, _, Leaf) -> Some v
  | TreeNode (_, _, right) -> max right

let rec min tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, Leaf, _) -> Some v
  | TreeNode (_, left, _) -> min left

let rec printInfix tree =
  match tree with
  | TreeNode (value, left, right) -> printInfix left; printf "%A" value; printInfix right;
  | Leaf -> ()

let rec printPostfix tree =
  match tree with
  | TreeNode (value, left, right) -> printInfix left; printInfix right; printf "%A" value; 
  | Leaf -> ()

let rec printPrefix tree =
  match tree with
  | TreeNode (value, left, right) -> printf "%A" value; printInfix left; printInfix right;
  | Leaf -> ()

let print tree order =
  match order with
  | INFIX -> printInfix tree
  | POSTFIX -> printPostfix tree
  | PREFIX -> printPrefix tree

let rec search value tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, left, right) ->
    if value = v then Some v
    elif value < v then search value left
    else search value right

let rec remove value tree =
  match tree with
  | Leaf -> tree
  | TreeNode (v, left, right) ->
    let nextTree = 
      if value < v then TreeNode (v, remove value left, right)
      elif value > v then TreeNode (v, left, remove value right)
      else
        match (left, right) with
        | (TreeNode (_), TreeNode (_)) as children -> 
          let tLeft, tRight = children
          match min tRight with
          | None -> tree
          | Some successor -> TreeNode (successor, tLeft, remove successor tRight)
        | (Leaf, Leaf) -> Leaf
        | (t, Leaf) | (Leaf, t) -> t

    match nextTree with
    | Leaf -> nextTree
    | TreeNode (v, l, r) ->
      let heightDiff = (height l) - (height r)

      if heightDiff > 1 then TreeNode (rightRotationStrategy (v, l, r))
      elif heightDiff < -1 then TreeNode (leftRotationStrategy (v, l, r))
      else nextTree