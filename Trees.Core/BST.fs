module BST

open Node
open Order

let rec add value tree =
  match tree with
  | Leaf -> TreeNode(value, Leaf, Leaf)
  | TreeNode (v, left, right) as node ->
      if value < v then TreeNode (v, add value left, right)
      elif value > v then TreeNode (v, left, add value right)
      else node

let create value =
  TreeNode (value, Leaf, Leaf)

let rec max tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, _, Leaf) -> Some v
  | TreeNode (_, _, right) -> max right

let rec min tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, Leaf, _) -> Some v
  | TreeNode (_, left, _) -> min left

let rec printInfix tree =
  match tree with
  | TreeNode (value, left, right) -> printInfix left; printf "%A" value; printInfix right;
  | Leaf -> ()

let rec printPostfix tree =
  match tree with
  | TreeNode (value, left, right) -> printInfix left; printInfix right; printf "%A" value; 
  | Leaf -> ()

let rec printPrefix tree =
  match tree with
  | TreeNode (value, left, right) -> printf "%A" value; printInfix left; printInfix right;
  | Leaf -> ()

let print tree order =
  match order with
  | INFIX -> printInfix tree
  | POSTFIX -> printPostfix tree
  | PREFIX -> printPrefix tree

let rec search value tree =
  match tree with
  | Leaf -> None
  | TreeNode (v, left, right) ->
    if value = v then Some v
    elif value < v then search value left
    else search value right

let rec remove value tree =
  match tree with
  | Leaf -> tree
  | TreeNode (v, left, right) ->
    if value < v then TreeNode (v, remove value left, right)
    elif value > v then TreeNode (v, left, remove value right)
    else
      match (left, right) with
      | (TreeNode (_), TreeNode (_)) as children -> 
        let tLeft, tRight = children
        match min tRight with
        | None -> tree
        | Some successor -> TreeNode (successor, tLeft, remove successor tRight)
      | (Leaf, Leaf) -> Leaf
      | (t, Leaf) | (Leaf, t) -> t