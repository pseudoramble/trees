module Node

type TreeNode<'T> = 
  | Leaf
  | TreeNode of 'T * TreeNode<'T> * TreeNode<'T>