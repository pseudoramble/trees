# Trees!

Trees are most grand. This is a spot to try and implement different kinds of trees.

## Existing Code

1. A Node module. It has a value, and two child nodes (left and right).
1. An Order module. It allows one to specify how to traverse a tree.

## Some "Rules" If You Will

1. Code will attempt to adhere to this interface: create v, add v, search v tree, remove v tree, min tree, max tree, print tree
1. No duplicate values will be included! When a duplicate value occurs we will ignore it.
1. The data structures will be immutable. So potentially not the fastest code, but safe (also fun to figure out).

## Lessons Learned

1. The type system is your friend. It's there to enforce properties outside of runtime when it can!
1. The type system is your friend. The design of it makes code fall out of it much more naturally.
1. Order is important (`v < value` is very different from `value < v`).
1. Organize your problem, design your system, type in code.
1. Pictures, a whiteboard, and your imagination help. Visualizing what is happening & why is good.
1. Copying is vital for purely functional data structure (if not done lazily)

## TODO

* (Done) Implement simple BST
* (In Progress) Implement tree printing (prefix, infix, postfix, visual/shape format)
* AVL BST
* Red-Black BST
* [Optional] Improve memory usage/tree performance